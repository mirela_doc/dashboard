import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdDashboardModule } from './ad-dashboard/ad-dashboard.module';

const routes: Routes = [{
  path: 'ads',
  loadChildren: () => AdDashboardModule
},
{
  path: '',
  redirectTo: '/ads',
  pathMatch: 'full'
},
{ path: '**', redirectTo: '/ads' }]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
