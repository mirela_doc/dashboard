import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdListComponent } from './ad-list/ad-list.component';
import { AdDetailComponent } from './ad-detail/ad-detail.component';
import { SharedModule } from '../shared/shared.module';
import { AdDashboardRoutingModule } from './ad-dashboard-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};


@NgModule({
  declarations: [
    AdListComponent,
    AdDetailComponent
  ],
  imports: [
    CommonModule,
    AdDashboardRoutingModule, 
    SharedModule,
    FontAwesomeModule,
    ModalModule.forRoot(),
    NgbModule,
    FormsModule,
    NgxMaskModule.forRoot(options)
  ]
})
export class AdDashboardModule { }
