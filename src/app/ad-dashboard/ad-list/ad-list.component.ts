import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AdsService } from 'src/app/core/ad.service';
import { Ad } from '../ad.model';
import { faPlusCircle, faCalendar, faSearch, faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AdDetailComponent } from '../ad-detail/ad-detail.component';
import { NgbdSortableHeader } from 'src/app/shared/sortable.directive';
import { SortEvent } from 'src/app/shared/sortEvent.model';
@Component({
  selector: 'das-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.scss']
})
export class AdListComponent implements OnInit {
  public bsModalRef: BsModalRef | undefined;
  public collectionSize = 25;
  public faCalendar = faCalendar;
  public faPlusCircle = faPlusCircle;
  public faSearch = faSearch;
  public faEllipsisV = faEllipsisV;
  public options = [
    { name: 'Last 30 Days', value: '0', checked: false },
    { name: 'Most expensive', value: '1', checked: false },
    { name: 'Last 100 Days', value: '3', checked: false },
  ];
  public page = 1;
  public pageSize = 5;
  public _searchString!: string;
  public sortedAds: Ad[];
  public submenu = [{ name: 'Sub Menu Link', to: '/ads', active: 'true' }, { name: 'Sub Menu Link', to: '/reports', active: 'false' },
  { name: 'Sub Menu Link', to: '/reports', active: 'false' }, { name: 'Sub Menu Link', to: '/reports', active: 'false' }, { name: 'Sub Menu Link', to: '/reports', active: 'false' },
  { name: 'Sub Menu Link', to: '/reports', active: 'false' }];


  private ads: Ad[];
  private filteredAds: Ad[];

  @ViewChildren(NgbdSortableHeader)
  headers!: QueryList<NgbdSortableHeader>;

  constructor(private adService: AdsService, private modalService: BsModalService) {
    this.ads = [];
    this.filteredAds = [];
    this.sortedAds = [];
  }

  ngOnInit(): void {
    this.getAds();
  }

  public onSort({ column, direction }: SortEvent) {

    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    if (direction === '' || column === '') {
      this.sortedAds = this.filteredAds;
    } else {
      this.sortedAds = [...this.filteredAds].sort((a, b) => {
        const res = this.compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  public count() {
    return this.selectedOptions.length;
  }

  public createAd() {
    this.bsModalRef = this.modalService.show(AdDetailComponent, {
    });
  }

  public get searchString(): string {
    return this._searchString;
  }

  public set searchString(value: string) {
    this._searchString = value;
    this.filteredAds = this.filterAds(value);
    this.sortedAds = this.filteredAds;
    this.headers.forEach(header => {
      header.direction = '';
    });
  }

  public trackByIndex(index: any) {
    return index;
  }

  private compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;


  private getAds() {
    this.adService.getAds().subscribe(ads => {
      this.ads = ads;
      this.sortedAds = ads;
      this.filteredAds = ads;
    });
  }

  private filterAds(searchString: string) {
    return this.ads.filter(ad =>
      ad.name.toLowerCase().includes(searchString.toLowerCase())
    );
  }

  private get selectedOptions() {
    return this.options
      .filter(opt => opt.checked)
      .map(opt => opt.value)
  }

}
