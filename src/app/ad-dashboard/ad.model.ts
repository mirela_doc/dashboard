export interface Ad {
  id: number;
  name: string;
  metric1: number;
  metric2: number;
  metric3: number;
  metric4: number;
}