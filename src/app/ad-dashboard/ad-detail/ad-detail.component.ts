import { Component } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AdsService } from 'src/app/core/ad.service';
import { Ad } from '../ad.model';

@Component({
  selector: 'das-ad-detail',
  templateUrl: './ad-detail.component.html',
  styleUrls: ['./ad-detail.component.scss']
})
export class AdDetailComponent {
  public ad: Ad = {} as Ad;

  constructor(public bsModalRef: BsModalRef, private adService: AdsService) { }

  public close() {
    this.bsModalRef.hide();
  }

  public save() {
    this.adService.createAd(this.ad).subscribe(ad => {
      //TODO Refresh data / Detail page redirect
      this.close();
    }, (err: {}) => {

    });
  }

}
