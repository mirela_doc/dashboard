import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdListComponent } from './ad-list/ad-list.component';


 const adDashboardRoutes: Routes = [
  {
    path: '',
    component: AdListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(adDashboardRoutes)],
  exports: [RouterModule]
})
export class AdDashboardRoutingModule {
}
