import {HostListener, Directive, EventEmitter, Input, Output} from '@angular/core';
import { SortColumn, SortDirection, SortEvent } from './sortEvent.model';

const rotate: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': '', '': 'asc' };

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {
  @HostListener('keydown', ['$event.target'])
  onKeyDown(e: { which: number; }) {
    if (e.which === 13) { //only react to "Enter"
      this.rotate();
    }
  }

  @Input() sortable: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.sortable, direction: this.direction});
  }
}
