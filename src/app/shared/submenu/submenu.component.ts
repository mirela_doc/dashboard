import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'das-submenu',
  templateUrl: './submenu.component.html',
  styleUrls: ['./submenu.component.scss']
})
export class SubmenuComponent {
  @Input()
  submenu!: any;

  constructor(private router: Router) { }


}
