import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SubmenuComponent } from './submenu/submenu.component';
import { RouterModule } from '@angular/router';
import { NgbdSortableHeader } from './sortable.directive';
@NgModule({
  declarations: [
    NavbarComponent,
    SubmenuComponent,
    NgbdSortableHeader
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [NavbarComponent, SubmenuComponent, NgbdSortableHeader]
})
export class SharedModule { }
