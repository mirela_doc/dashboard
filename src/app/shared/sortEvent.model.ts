import { Ad } from "../ad-dashboard/ad.model";


export type SortColumn = keyof Ad | '';
export type SortDirection = 'asc' | 'desc' | '';

export interface SortEvent {
    column: SortColumn;
    direction: SortDirection;
  }
  