import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})

export class DataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    return {
      ads: [
        {
          id: 1,
          name: 'Object Name 1',
          metric1: 207724,
          metric2: 2580,
          metric3: 0,
          metric4: 7.85

        },
        {
          id: 2,
          name: 'Object Name 2',
          metric1: 397644,
          metric2: 5930,
          metric3: 5139,
          metric4: 11.31

        },
        {
          id: 3,
          name: 'Object Name 3',
          metric1: 1397644,
          metric2: 10500,
          metric3: 7381,
          metric4: 2.99

        },
        {
          id: 4,
          name: 'Object Name 4',
          metric1: 5900644,
          metric2: 320000,
          metric3: 87321,
          metric4: 1.51

        },
         {
          id: 5,
          name: 'Object Name 5',
          metric1: 207724,
          metric2: 51600,
          metric3: 0,
          metric4: 0
        }
      ]
    };
  }
}